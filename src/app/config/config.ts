export class Config {
  customerDiscount: boolean;
  stageName: string;
  simulationDate: string;
  totalCustomerCountPerDay: number;
  stageIsEnabled: boolean;
  numberOfProducts: number;
  id: number;
  cumulativeCustomerDiscount: boolean;
  maxProductsPerOrder: number;
  selectRandomProducts: boolean;
}
