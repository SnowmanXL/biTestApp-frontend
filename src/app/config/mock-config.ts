import {Config} from './config';

export const CONFIG: Config[] = [
  {
    customerDiscount: false,
    stageName: 'Stage1',
    simulationDate: '01-01-2017',
    totalCustomerCountPerDay: 25000,
    stageIsEnabled: true,
    numberOfProducts: 3,
    id: 1,
    cumulativeCustomerDiscount: false,
    maxProductsPerOrder: 4,
    selectRandomProducts: true
  },
  {
    customerDiscount: false,
    stageName: 'Stage1',
    simulationDate: '01-01-2017',
    totalCustomerCountPerDay: 25000,
    stageIsEnabled: true,
    numberOfProducts: 3,
    id: 2,
    cumulativeCustomerDiscount: false,
    maxProductsPerOrder: 4,
    selectRandomProducts: true
  },
  {
    customerDiscount: false,
    stageName: 'Stage1',
    simulationDate: '01-01-2017',
    totalCustomerCountPerDay: 25000,
    stageIsEnabled: true,
    numberOfProducts: 3,
    id: 3,
    cumulativeCustomerDiscount: false,
    maxProductsPerOrder: 6,
    selectRandomProducts: true
  }
];
